while getopts 't:h' opt; do
  case "$opt" in
      t)
          ansible-playbook -t ${OPTARG} -i testing_inventory --user ansible --diff --vault-pass-file /etc/ansible/vault_pw --private-key ~/.ssh/id_rsa --become-user root ../local.yml
          exit 0
          ;;
      h)
          echo "Usage: $(basename $0) [-t TAGS]"
          exit 0
          ;;
      :)
          echo -e "Option requires an argument.\nUsage: ./$(basename $0) [-t TAGS]"
          exit 1
          ;;
      ?)
          echo -e "Option requires an argument.\nUsage: ./$(basename $0) [-t TAGS]"
          exit 1
          ;;
  esac
done

ansible-playbook -i testing_inventory --user ansible --diff --vault-pass-file /etc/ansible/vault_pw --private-key ~/.ssh/id_rsa --become-user root ../local.yml
