# Personal Configuration Management Example
An ansible-pull repo configuring a whole network including desktops, laptops and servers.

This example repo is not intended to be run against any real hosts. Feel free to get inspired for your own infrastructure.

Currently supports Debian Stable and Raspbian OS.

# Bootstrapping a Machine
- Mount all drives you need, and setup fstab
- Extend your apt sources with **contrib** and **non-free**
- Add the machine's hostname to the desired groups in the ansible hosts file `hosts/all`
- Set the machine's host variables in `host_vars/<hostname>.yml` for each group you added the host to. Host variable defaults can be seen in `roles/<role>/defaults/*`
- install sudo: `apt install sudo`
- download the bootstrap script **debian-bootstrap.sh** e.g. by tftp/http and run it: `sudo ./debian-bootstrap.sh`

# Roles
- base: Run on every machine
- workstation: Tasks run on workstations, installs display manager, window manager
and software
- server: Tasks run on every server like security precautions
- sftp-server: Setup an SFTP server with OpenSSH
- git-server: Setup a Git Server which uses the SSH protocol
- tftp-server: Setup a TFTP server

# Role Variables
## base

| Variable | Datatype | Default Value | Description |
| ------------- | ------------- | - | - |
| PS1  | String  |  | Shell Prompt |
| has_amd_cpu  | Boolean  | false | |
| has_intel_cpu  | Boolean  | true | |
| is_raspberry_pi  | Boolean  | false | |
| is_wireless  | Boolean  | false | |
| install_doas  | Boolean  | false | |

## server

## workstation

| Variable | Datatype | Default Value | Description |
| ------------- | ------------- | - | - |
| display_manager  | String  | lightdm  | Choose between 'ly' or 'lightdm' |

## sftp-server

| Variable | Datatype | Default Value | Description |
| ------------- | ------------- | - | - |
| SFTP_GROUP_NAME  | String  | sftp_users | Name of the sftp user group |
| SFTP_DATA_DIR  | String  | /sftp-data | Path of the sftp data directory |
| SFTP_USERS_AND_PASSWORDS  | String-String Dictionary  | e.g. "sftp_testuser: $6$salt$your_pw_hash"  | Users to create and corresponding password hash |
|   |   |   | |

## git-server

| Variable | Datatype | Default Value | Description |
| ------------- | ------------- | - | - |
| GIT_DATA_DIR  | String  | /srv/git | Path of git data directory |
| CREATE_GIT_REPOS  | String List  |  - example  | Repos to create and initiate |
|   |   |   | |


## tftp-server

| Variable | Datatype | Default Value | Description |
| ------------- | ------------- | - | - |
| TFTP_USERNAME  | String  | tftp  | |
| TFTP_GROUPNAME  | String  | tftp  | |
| TFTP_DATA_DIRECTORY  | String  | /srv/tftp  | Path to tftp data directory |
| TFTP_ADDRESS  | String  | :69  | Listening IP and Port, e.g. ':69' or '127.0.0.1:69' |
| TFTP_OPTIONS  | String  | --secure --create --verbosity 5 | Options to parse to the tftp daemon, see man tftpd of tftp-hpa packet |
|   |   |   | |