#!/usr/bin/env bash
# run with: "curl <file> | sudo bash"
#
# Will do the following:
#   1. update apt
#   2. install ansible-pull and ansible-vault
#   3. ask you for the vault key and save it to a file
#   4. pull and apply my playbook which includes setting up a cronjob for
#      further automated provisioning

# Variables
ansible_user=ansible
vault_key_dir="/etc/ansible"
vault_key_filename="vault_key"

# update package index
sudo apt update -y

# install ansible-pull, ansible-vault
sudo apt install -y ansible git python3-apt

# Vault Key
# Source: https://stackoverflow.com/a/3980713
# prepare file
if ! [ -f ${vault_key_dir}/${vault_key_filename} ]; then
    sudo mkdir -p ${vault_key_dir}
    sudo touch ${vault_key_dir}/${vault_key_filename}
    sudo chmod 600 ${vault_key_dir}/${vault_key_filename}
    # read password
    trap 'stty echo' INT # prevent breaking after Ctrl-C
    stty -echo
    printf "Ansible Vault Password: "
    read vault_key_pw
    sudo sh -c "echo $vault_key_pw > ${vault_key_dir}/${vault_key_filename}"
    unset vault_key_pw
    stty echo
    printf "\n"
fi
# Bootstrap
sudo ansible-pull -o --vault-password-file /etc/ansible/vault_key -U yourrepo.git
